import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    dict_headers = {"Authorization ": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    url = f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1"
    
    # Make the request
    data = requests.get(url, headers=dict_headers)
    # Parse the JSON response
    data = data.json()
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": data["photos"][0]["src"]["original"]}
    except (KeyError, TypeError):
        return f"Error: {data.status_code} - {data.reason}"
    
    
def get_weather_data(city, state):
    params = {"query": (city, state),
              "appid": OPEN_WEATHER_API_KEY,
              "limit": 1,
              }
    url = "https://api.openweathermap.org/data/2.5/weather"
    
    response = requests.get(url, params=params)
    
    coordinates = response.json()
    
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    
    weather_params = {"lat": coordinates[0]["lat"],
                      "lon": coordinates[0]["lon"],
                      "appid": OPEN_WEATHER_API_KEY,
                      "units": "imperial",
                      }
    weather_response = requests.get(weather_url, params=weather_params)
    
    local_weather = weather_response.json()
    
    try:
        return {
            "temperature": local_weather["main"]["temp"],
            "description": local_weather["weather"][0]["description"],
        }
        
    except (KeyError, TypeError):
        return f"Error: {response.status_code} - {response.reason}"
    

    